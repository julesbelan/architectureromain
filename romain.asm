# but: transformer un nombre entre 1 et 3999 en notation décimale
# date: 24-MA-2019
# auteurs: Julien Bélanger, Mike Useni
# adresses de courriel: julesbelan@gmail.com, usenimikesefu@gmail.com
# codes permanents: p1088873, p1130316

# romain.asm
# Fonctions résumées:
#		main: demande à l'usager son nombre appelle romain puis imprime la chaîne construite par romain
#		romain: boucle sur chaque puissance du nombre et la tranforme en notation décimale
#		chiffre: effectue le changement de puissance à romain de façon dictionnaire
#		repeter: repete un symbole un nombre fini de fois

# Client qui prend un nombre e [1,3999] et appelle la fonction romain dessus imprime la chaîne construite par romain et termine
# utilise les registres
#	$a0: nb à encoder (i.e: 439)
# 	$s1: rang nb (i.e: 100)
# 	$s2: adresse sous-chaîne (i.e adr. de "C")
# 	$a1: adresse de résultat buffer
main:

	# Demander à l'utilisateur un nombre entre 1 et 3999
	li $v0, 4
	la $a0, msg
	syscall 
	
	# Saisir une valeur à l'entrée standard et la stockée dans $r0
	li $v0, 5
	syscall 
	add $a0, $v0, $0
	
	# appeler romain
	jal romain
	
	# imprimer buffer
	li $v0, 4
	la $a0, buffer
	syscall
	
	#terminer le programme
	li $v0, 10
	syscall

romain: 
	# partir le programme par les 1000 
	addi $s1, $0, 1000
	la $s2, M
	la $a1, buffer

	# boucle while 
	while:
	# tant que mon nombre n'est pas égale à 0 (à mesure que l'on calcule la séquence 
	# on enlève les puissances ex: in: 3287 (loop 1) 3287 (loop 2) 287 (loop 3) 87
	# (loop 4) 7 (loop 5) 0 --> fini 
	beq $a0, 0, done	
	# prévoyant des subroutines, on garde $ra dans le stack pointer sp
	addi $sp, $sp, -4 # vers le bas
	sw $ra, 0($sp) # mettre $ra dans sp
	jal chiffre
	# reassigner le $ra original gardé dans $sp
	lw $ra, 0($sp)
	add $sp, $sp, 4 # réincrémenter le sp
	div $s1, $s1, 10 # s1 = s1/10
	la $s2, -2($s2) # move cursor from M to C or from C to X or from X to I
	j while
	
	done:	
	jr $ra



# Utilise les registres:
# 	$s3: nb repetitions 
# 	$s4: adresse symbole d'encodage
#	$a1: adresse de résultat buffer
repeter:
	la $t1, ($s4)	# loader l'adresse du symbole dans $t1 (overwrite par la même occasion)
	li $t0, 0	# mettre incrémenteur à 0
	initlp:
		beq $t0, $s3, initdn # si i >= listsz = $s0 = 90 alors allez à initdn
		lb $t2, ($s4) # mettre l'octet charactere dans $t2 (mettre charatere dans $t2)
		sb $t2, ($a1) # mettre la valeur de charactere à l'adresse de $s1 (mettre charactere dans $s1)
		addi $t0, $t0, 1 # i = i + 1
		addi $a1, $a1, 1 # $s1 = $s1 + 1
		j initlp # allez à initdn
	initdn: #addi $a1, $a1, 1 #augmenter l'adresse de 1 (on doit retourner la prochaine adresse
		jr $ra #return vers où on était dans chiffre

# Utilise les registres:
# 	$t0: contient le résultat de la comparaison 1=true/0=false
# 	$s0 contient le résultat de la multiplication entre rang_nombre et un chiffre entre 1 et 9
chiffre:
	
	#trouver quelle est le nombre 
	# if (nb < 4)
	#ifNbL4:
	mul $s0, $s1, 4 # multiplier 4 et le rang et le mettre dans $s0
	slt $t0, $a0, $s0 # si nombre < 4 * rang du nombre
	beq $t0, $0 ifNbL5 # sinon brancher à ifNbL5
		# if (nb < 1)
		slt $t0, $a0, $s1 # Si nombre < 1 * rang du nombre
		beq $t0, $0 ifNbL2 # sinon brancher à ifNbL2
		# do 
		jr $ra # brancher à romain 
		
		# if (nb < 2)
		ifNbL2:
		mul $s0, $s1, 2
		slt $t0, $a0, $s0
		beq $t0, $0,  ifNbL3
		# do 
		sub $a0, $a0, $s1 # nombre à encoder = nombre à encoder - 1*rang_nb
		lb $t1, ($s2)
		sb $t1, ($a1)
		addi $a1, $a1, 1	
		jr $ra

		# if (nb < 3)
		ifNbL3:
		mul $s0, $s1, 3
		slt $t0, $a0, $s0
		beq $t0, $0,  nb3
		# do
		mul $s0, $s1, 2
		sub $a0, $a0, $s0 # nombre à encoder = nombre à encoder - 2*rang_nb
		addi $s3, $0, 2	# repeter(2, ($s2), ... )
		la $s4, ($s2)
		
		addi $sp, $sp, -4 # vers le bas
		sw $ra, 0($sp)
		jal repeter
		lw $ra, 0($sp)
		add $sp, $sp, 4
		
		jr $ra
				
		# nb == 3
		nb3:
		mul $s0, $s1, 3
		sub $a0, $a0, $s0 # nombre à encoder = nombre à encoder - 3*rang_nb
		addi $s3, $0, 3 # repeter (3, ($s2), ... )
		la $s4, ($s2)
		
		addi $sp, $sp, -4 # vers le bas
		sw $ra, 0($sp)
		jal repeter
		lw $ra, 0($sp)
		add $sp, $sp, 4
		
		jr $ra
	
	# if (nb < 5)
	ifNbL5:
	mul $s0, $s1, 5 # $s0 = $s1 (aka rang_nb) * $s0 (aka 5)
	slt $t0, $a0, $s0 # tester si nb < $s0
	beq $t0, $0 ifNbL9 # sinon brancher à ifnbl9
	mul $s0, $s1, 4 	# $s0 = 4 * rang_nb
	sub $a0, $a0, $s0 # nb à encoder = nb à encoder - 4*rang_nb
	lb $t1, ($s2) # ajouter I (ou rang_équivalent)
	sb $t1, ($a1)
	lb $t1, 1($s2) # ajouter V (ou rang_équivalent)
	sb $t1, 1($a1)
	addi $a1, $a1, 2 # buffer pointer avance de 2
	jr $ra
	 
	
	# if (nb < 9)
	ifNbL9:
	mul $s0, $s1, 9
	slt $t0, $a0, $s0
	beq $t0, $0, is9
	lb $t1, 1($s2) # ajouter V
	sb $t1, ($a1)
	addi $a1, $a1, 1 #avancer pointeur dans buffer
		# if (nb < 6)
		mul $s0, $s1, 6 # $s0 = $s1 (aka rang) * $s0 (aka 6)
		slt $t0, $a0, $s0
		beq $t0, $0, ifNbL7
		mul $s0, $s1, 5
		sub $a0, $a0, $s0 # nombre à encoder = nombre à encoder - 5*rang_nb
		jr $ra # return
		
		# else if (nb < 7)
		ifNbL7:
		mul $s0, $s1, 7
		slt $t0, $a0, $s0
		beq $t0, $0, ifNbL8
		mul $s0, $s1, 6
		sub $a0, $a0, $s0 # nombre à encoder = nombre à encoder - 6*rang_nb
		addi $s3, $0, 1
		la $s4, ($s2)
		addi $sp, $sp, -4 # vers le bas
		sw $ra, 0($sp)
		jal repeter
		lw $ra, 0($sp)
		add $sp, $sp, 4
		jr $ra
		
		# else if (nb < 8)
		ifNbL8:
		mul $s0, $s1, 8
		slt $t0, $a0, $s0
		beq $t0, $0, is8
		mul $s0, $s1, 7
		sub $a0, $a0, $s0
		addi $s3, $0, 2
		la $s4, ($s2)
		addi $sp, $sp, -4 # vers le bas
		sw $ra, 0($sp)
		jal repeter
		lw $ra, 0($sp)
		add $sp, $sp, 4
		jr $ra
		
		# nb == 8
		is8:
		mul $s0, $s1, 8
		sub $a0, $a0, $s0
		addi $s3, $0, 3
		la $s4, ($s2)
		addi $sp, $sp, -4 # vers le bas
		sw $ra, 0($sp)
		jal repeter
		lw $ra, 0($sp)
		add $sp, $sp, 4
		jr $ra
		
	# else  	
	is9:
	mul $s0, $s1, 9
	sub $a0, $a0, $s0
	lb $t1, ($s2) # ajouter I
	sb $t1, ($a1)
	lb $t1, 2($s2) # ajouter X
	sb $t1, 1($a1)
	addi $a1, $a1, 2
	jr $ra
		
.data 
buffer:	.space 30
I:	.ascii "I"
V:	.ascii "V"
X:	.ascii "X"
L:	.ascii "L"
C:	.ascii "C"
D:	.ascii "D"
M:	.ascii "M"
msg:	.asciiz "Entrer un nombre de 1 à 3999:"